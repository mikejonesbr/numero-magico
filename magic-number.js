// let 
// const -> Const é uma variável que não pode ter re atribuições(Reassign)
// var 

// string
// number
// boolean
// object
// underfined
// function

function reset (title, retryButton, tryList, attemptEl, btnMagicNumber){
    title.remove()
    retryButton.remove()
    tryList.innerHTML = ''
    attempt = 0
    attemptEl.innerHTML = maxAttempt
    btnMagicNumber.removeAttribute('disabled')
    secretNumber = Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber)

}

function getRandomNumber(){
    return Math.round(Math.random() * (maxSecretNumber - minSecretNumber) + minSecretNumber)

}

function renderReset(titleText, btnMagicNumber, mainContainer, onReset){
    const title = document.createElement('h2')
    const retryButton = document.createElement('button')
    
    title.innerText = titleText
    retryButton.innerText = 'Tentar Novamente'
    retryButton.addEventListener('click', function(){
        onReset(title, retryButton)
    })
    mainContainer.append(title)
    mainContainer.append(retryButton)
    btnMagicNumber.setAttribute('disabled', false)
}



const btnMagicNumber = document.getElementById('btn-magic-number')
const inputMagicNumber = document.getElementById('magic-number')
const maxAttemptEl = document.getElementById('magic-number');
const attemptEl = document.getElementById('attempt')
const tryList = document.getElementById('try-list')
const mainContainer = document.getElementsByTagName('body')[0]
const barProgress = document.getElementById('barra-progresso')


const maxAttempt = 10
const maxSecretNumber = 100
const minSecretNumber = 1

let secretNumber = getRandomNumber()
let attempt = 0
let isWinner = false
console.log(secretNumber)

maxAttemptEl.innerHTML = maxAttempt

btnMagicNumber.addEventListener("click", function(){
    attempt += 1
    attemptEl.innerHTML = maxAttempt - attempt
    barProgress.value = attempt
    const magicNumber = Math.floor(+inputMagicNumber.value)
    inputMagicNumber.value =''
    inputMagicNumber.focus()
   
    /* renderizandoa lista por completo
    tryList.innerHTML = `<li>${magicNumber}</li`
    */

    // Renderizando um item apenas.
   const tryItemElement = document.createElement('li')
   tryItemElement.innerHTML = magicNumber
   if(magicNumber > secretNumber){
       tryItemElement.classList.add('higher-number')
   }else if (magicNumber < secretNumber){
       tryItemElement.classList.add('lowest-number')
   }else {
       tryItemElement.classList.add('correct-number')
       isWinner = true
   }


   tryList.append(tryItemElement)
   if(attempt >= 10 || isWinner){
        let titleText = isWinner ? 'Você Ganhou!' : 'Você perdeu'
        renderReset(
            titleText,
            btnMagicNumber,
            mainContainer,
            function(title, retryButton){
                reset(
                    title,
                    retryButton,
                    tryList,
                    attemptEl,
                    btnMagicNumber,
                    maxAttempt
                )

                attempt = 0
                secretNumber = getRandomNumber()
                isWinner = false
                console.log(secretNumber)
            }
        )
    }
});
